package com.smartbear.uxm.test.smoketest;

import java.util.List;
import java.util.Random;

import junit.framework.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Real extends Driver {

    public static void searchForApplication(String applicationName) throws Exception {
	Instance.findElement(By.name("searchInput")).clear();
	Instance.findElement(By.name("searchInput")).sendKeys(applicationName);

    }

    public static void verifyRealTransactionTrace() throws Exception {

	// Login and navigate to Issues - > Summary Tab
	LoginTests.performLogin();
	Navigation.openScreenReal();
	Navigation.openIssuesSummaryTab();

	// Select issue that has the biggest amount of occurrences
	WebElement issuesTable = Instance.findElement(By
		.xpath("//div[@id='root_cause_problems_list']/div[2]/div/div[2]/div[2]/div/table/tbody"));
	List<WebElement> issuesList = issuesTable.findElements(By.tagName("tr"));
	WebElement topIssue = issuesList.get(0);
	topIssue.click();

	// Verify that Issue Occurrences grid is clickable and select top
	// occurrence
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@id='root_cause_defects_list_from_problems']/div[2]/div/div[2]/div[2]/div/table/tbody/tr")));
	WebElement issuesOccurrencesTable = Instance.findElement(By
		.xpath("//div[@id='root_cause_defects_list_from_problems']/div[2]/div/div[2]/div[2]/div/table/tbody"));
	List<WebElement> issuesOccurrencesList = issuesOccurrencesTable.findElements(By.tagName("tr"));
	WebElement topOccurrence = issuesOccurrencesList.get(0).findElement(By.xpath("//td[7]/span/a/span"));
	topOccurrence.click();

	// Ensure that Occurrences tab opened and decompile the top method
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@id='root_cause_transaction_slowest_modules']/div[2]/div/div[2]/div[2]/div/table/tbody/tr")));
	WebElement slowestMethodsTable = Instance.findElement(By
		.xpath("//div[@id='root_cause_transaction_slowest_modules']/div[2]/div/div[2]/div[2]/div/table/tbody"));
	List<WebElement> slowestMethodsList = slowestMethodsTable.findElements(By.tagName("tr"));
	WebElement topMethod = slowestMethodsList.get(0).findElement(By.xpath("//td[6]/span/span"));
	topMethod.click();

	// Wait for Code View modal window open and verify text
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='loader']/div[2]/button")));
	Assert.assertTrue("No transaction trace data in real", transactionTraceIsVisible() != false);
    }

    public static Boolean transactionTraceIsVisible() {
	String ttState = Instance.findElement(By.xpath("//div[@class='cardContent']/div/ol/li[1]/span/span")).getText();
	if (ttState.contentEquals("/* IdVerifier - Decompiled by JODE")) {
	    return true;
	} else {
	    return false;
	}
    }

    public static void verifyAgentUpStatus() throws Exception {

	LoginTests.performLogin();
	Navigation.openScreenReal();
	Navigation.openAgentsTab();
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@class='richDataTable ng-scope']/table/tbody/tr")));
	WebElement agentSearchBox = Instance.findElement(By.name("searchInput"));
	String agentName = "ubuntu";
	agentSearchBox.sendKeys(agentName);
	Assert.assertTrue("Agent is down", agentIsUp() != false);

    }

    public static Boolean agentIsUp() {
	String agentStatus = Instance.findElement(
		By.xpath("//div[@class='richDataTable ng-scope']/table/tbody/tr/td[6]")).getText();
	if (agentStatus.contentEquals("Up")) {
	    return true;
	} else {
	    return false;
	}

    }

    public static void createNewSLA(String slaName) throws Exception {
	LoginTests.performLogin();
	Navigation.openScreenReal();
	Navigation.openManageSlasTab();
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@class='richDataTable ng-scope']/table/tbody/tr")));
	WebElement firstSLA = Instance.findElement(By.xpath("//div[@class='richDataTable ng-scope']/table/tbody/tr"));
	Instance.findElement(By.xpath("//div[@id='root_cause_manage_slas']/div[2]/div/div[2]/div/div[2]/button"))
		.click();
	wait.until(ExpectedConditions.elementToBeClickable(By.name("name")));
	Instance.findElement(By.name("name")).clear();
	Instance.findElement(By.name("name")).sendKeys(slaName);
	Instance.findElement(By.name("type")).click();
	Instance.findElement(By.name("tolerating")).clear();
	Instance.findElement(By.name("tolerating")).sendKeys("500");
	Instance.findElement(By.xpath("//div[2]/button[2]")).click();
	wait.until(ExpectedConditions.elementToBeClickable(firstSLA));
	WebElement slaSearchBox = Instance.findElement(By.name("searchInput"));
	slaSearchBox.sendKeys(slaName);
	Assert.assertTrue("SLA was not created", firstSLA != null);

    }
    

    public static void deleteSLA(String slaName) throws Exception {
	LoginTests.performLogin();
	Navigation.openScreenReal();
	Navigation.openManageSlasTab();
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@class='richDataTable ng-scope']/table/tbody/tr")));
	WebElement slaSearchBox = Instance.findElement(By.name("searchInput"));
	slaSearchBox.sendKeys(slaName);
	WebElement editSLAButton = Instance.findElement(By
		.xpath("//div[@class='richDataTable ng-scope']/table/tbody/tr/td[5]/span/i"));
	editSLAButton.click();
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='leftContainer']/a/span"))).click();
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@class='leftContainer']/a/span"))).click();

    }
}
