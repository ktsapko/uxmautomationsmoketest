package com.smartbear.uxm.test.smoketest;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.*;

public class Navigation extends Driver {

    WebDriverWait wait;
    WebElement element;

    public static void selectMonitorFromDropdown(String monitorName) {

	Instance.findElement(By.xpath("//div[@class='listSelect']")).click();
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='searchInput']")));
	Instance.findElement(By.name("searchInput")).clear();
	Instance.findElement(By.name("searchInput")).sendKeys(monitorName);
	// Assert.assertTrue(Instance.findElement(By.xpath("//table[@class='table richDataTable ng-scope']")).isDisplayed());
	WebElement searchResults = Instance.findElement(By
		.xpath("//table[@class='table richDataTable ng-scope']/tbody"));
	List<WebElement> monitorsList = searchResults.findElements(By.tagName("tr"));
	WebElement properMonitor = monitorsList.get(0);
	properMonitor.click();
    }

    // Open Synthetic Screen
    public static void openScreenSynthetic() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='app-nav']/ul/li[2]/a/span/span")).click();
    }

    // Wait for Synthetic Dashboard To Be Load Completely
    public static void waitForSyntheticDashboard(long seconds) {
	WebDriverWait wait = new WebDriverWait(Driver.Instance, seconds);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='app-nav']/ul/li[5]/a/span/span")));

    }

    // Open Synthetic Dashboard Tab
    public static void openSyntheticDashboardTab() throws Exception {
	Driver.Instance.findElement(By.cssSelector("a.navigation-item-link.ng-scope > span.ng-binding")).click();
    }

    // Open Monitors Runs Tab
    public static void openMonitorsRuns() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[2]/a/span"))
		.click();
	Driver.Instance.findElement(By.linkText("Runs")).click();
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	// Instance.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='listSelect']")));
    }

    // Open Monitors Availability Tab
    public static void openMonitorsAvailability() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[2]/a/span"))
		.click();
	Driver.Instance.findElement(
		By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[2]/ul/li[3]/a/span")).click();
    }

    // Open Monitors Performance Tab
    public static void openMonitorsPerformance() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[2]/a/span"))
		.click();
	Driver.Instance.findElement(
		By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[2]/ul/li[4]/a/span")).click();
    }

    // Open SLAs Summary Tab
    public static void openSLAsTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[3]/a/i[2]"))
		.click();
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[3]/ul/li/a/span"))
		.click();
    }

    // Open SLAs Op Periods/Exclusions
    public static void openSlaPeriods() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[3]/a/i[2]"))
		.click();
	Driver.Instance.findElement(
		By.xpath("/html/body/div[2]/div[2]/div[2]/div[1]/div/div[1]/ul/li[3]/ul/li[2]/a/span")).click();
    }

    // Open Errors Tab
    public static void openErrorsTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[4]/a/span"))
		.click();
    }

    // Open Charting Tab
    public static void openChartingTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[5]/a/span"))
		.click();
    }

    // Open Performance Reports Tab
    public static void openPerformanceReportsTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[6]/a/span"))
		.click();
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[6]/ul/li/a/span"))
		.click();
    }

    // Open Document Manager in Reports Tab
    public static void openReportsDocumentManagerTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[6]/a/span"))
		.click();
	Driver.Instance.findElement(
		By.xpath("//div[@id='ui-content-area']/div[@id='ui-content-area']/div[2]/div/div/div/ul/li[6]/a/span"))
		.click();
    }

    // Open Alert Recipients Tab
    public static void openAlertRecipientsTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[7]/a/i[2]"))
		.click();
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[7]/ul/li/a/span"))
		.click();
    }

    // Open Alert Log Tab
    public static void openAlertLogTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[7]/a/i[2]"))
		.click();
	Driver.Instance.findElement(
		By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[7]/ul/li[2]/a/span")).click();
    }

    // Open Recipient Groups Tab
    public static void openRecipientGroupsTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[7]/a/i[2]"))
		.click();
	Driver.Instance.findElement(
		By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[7]/ul/li[3]/a/span")).click();
    }

    // Open User's Account Page
    public static void openUserAccountTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[8]/a")).click();
	Driver.Instance.findElement(By.linkText("Account")).click();
    }

    // Open Users' Plans Page
    public static void openUserPlansTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[8]/a")).click();
	Driver.Instance.findElement(By.linkText("Plans")).click();
    }

    // Open Users' Usage Statistics Page
    public static void openUsageStatisticsPage() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[8]/a")).click();
	Driver.Instance.findElement(
		By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[8]/ul/li[3]/a/span")).click();
    }

    // Open Real screen
    public static void openScreenReal() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='app-nav']/ul/li[3]/a/span/span")).click();
	WebDriverWait wait = new WebDriverWait(Instance, 30);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='cardContent']")));
    }

    // Open Real Dashboard Screen
    public static void openRealDashboardTab() throws Exception {

	try {
	    Driver.Instance.findElement(By.cssSelector("a.navigation-item-link.ng-scope > span.ng-binding")).click();
	    WebDriverWait wait = new WebDriverWait(Instance, 30);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("searchInput")));
	} catch (Exception e)

	{
	    System.out.println("Exception Is" + e);
	}

    }

    // Search for application by name
    public static void searchForApplication(String applicationName) {
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.name("searchInput"))).clear();
	// Driver.Instance.findElement(By.name("searchInput")).clear();
	Driver.Instance.findElement(By.name("searchInput")).sendKeys("applicationName");
    }

    // Switch dashboard to grid view
    public static void switchRealToGridView() {
	Driver.Instance
		.findElement(
			By.xpath("//div[@id='root_cause_dashboard']/div[2]/div/div[2]/div[2]/div/div/div/div/div/div[5]/button[2]"))
		.click();
    }

    // Switch dashboard to table view
    public static void switchRealToTableView() {
	Driver.Instance
		.findElement(
			By.xpath("//div[@id='root_cause_dashboard']/div[2]/div/div[2]/div[2]/div/div/div/div/div/div/div[3]/button"))
		.click();
    }

    // Open Topology Tab
    public static void openTopologyTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[2]/a/span"))
		.click();
    }

    // Switch Topology to tree view
    public static void switchTopologyToTreeView() {
	Driver.Instance.findElement(By.xpath("(//button[@type='button'])[3]")).click();
    }

    // Switch Topology to radial view
    public static void switchTopologyToRadialView() {
	Driver.Instance.findElement(By.cssSelector("button.btn")).click();
    }

    // Open Applications Summary Tab
    public static void openApplicationsSummaryTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[3]/a/span"))
		.click();
	Driver.Instance.findElement(By.cssSelector("a.sub-navigation-item-link > span.ng-binding")).click();
    }

    // Open Applications Interactions Tab
    public static void openApplicationsInteractionsTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[3]/a/i[2]"))
		.click();
	Driver.Instance.findElement(
		By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[3]/ul/li[2]/a/span")).click();
    }

    // Open RUM Tab
    public static void openRumTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[4]/a")).click();
    }

    // Open Resources Tab
    public static void openResourcesTab() throws Exception {
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[5]/a/span"))
		.click();
    }

    // Open Issues Summary Tab
    public static void openIssuesSummaryTab() throws Exception {
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[6]/a/span"))
		.click();
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[6]/ul/li/a/span")));
	Driver.Instance.findElement(By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[6]/ul/li/a/span"))
		.click();
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@id='root_cause_defects_list_from_problems']/div[2]/div/div[2]/div[2]/div/table/tbody/tr")));
    }

    // Open Issues Occurences Tab
    public static void openIssuesOccurencesTab() throws Exception {
	Driver.Instance.findElement(By.linkText("Issues")).click();
	Driver.Instance.findElement(
		By.xpath("//div[@id='ui-content-area']/div[2]/div/div/div/ul/li[6]/ul/li[2]/a/span")).click();
    }

    // Open Code Changes Tab
    public static void openCodeChangesTab() throws Exception {
	Driver.Instance.findElement(By.linkText("Code Changes")).click();
    }

    // Open Manage SLAs Tab
    public static void openManageSlasTab() throws Exception {
	Driver.Instance.findElement(By.linkText("Settings")).click();
	Driver.Instance.findElement(By.linkText("Manage SLAs")).click();
    }

    // Open Agents Tab
    public static void openAgentsTab() throws Exception {
	Driver.Instance.findElement(By.linkText("Settings")).click();
	Driver.Instance.findElement(By.linkText("Agents")).click();
    }

    public static void openRunsTab() {
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[2]/ul/li[2]/a/span"))).click();
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//body/div[2]/div[2]/div[2]/div[2]/div/div[3]/table/tbody")));

    }

}
