package com.smartbear.uxm.test.smoketest;

import org.junit.*;

public class SmokeTest extends LoginTests {

    // Environment URL
    static String baseUrl = "https://uxm.qac.alertsite.com/ui";
    static String apiMonitorName = "API monitor by WebDriver";
    static String webSiteMonitorName = "Web Site monitor by WebDriver";
    static String sslWebSiteMonitorName = "SSL Web Site monitor by WebDriver";
    static String dejaClickMonitorName = "JiraItemCreation";
    static String applicationName = "jira";
    static String slaName = "New sla by webdriver";

    // @Test
    public void createWebSiteMonitor() throws Exception {

	SyntheticDashboardPage.createWebSiteMonitor(webSiteMonitorName, "http://google.com", "15", "20");
    }

    // @Test
    public void createSecureWebsiteMonitor() throws InterruptedException {
	SyntheticDashboardPage.createSecureWebsiteMonitor(sslWebSiteMonitorName,
		"https://www.eff.org/Https-everywhere", "15", "20");
    }

    // @Test
    public void createAPIMonitor() throws InterruptedException {
	SyntheticDashboardPage.createAPIMonitor(apiMonitorName, "30", "5");
    }

    // @Test
    public void verifyApplicationDataInReal() throws Exception {
	RealTile.verifyApplicationDataInRealTile();

    }

    // @Test
    public void enableTransactionTrace() {
	try {
	    MonitorSummary.enableTransactionTrace(dejaClickMonitorName);
	} catch (InterruptedException e) {

	    e.printStackTrace();
	}
    }

     @Test
    public void verifySyntheticTransactionTraceData() throws Exception {
	MonitorRuns.verifySyntheticTransactionTraceData(dejaClickMonitorName);
    }

    // @Test
    public void runTestOnDemand() throws InterruptedException {
	MonitorSummary.runTestOnDemand(webSiteMonitorName);
    }

    // @Test
    public void verifyRealTransactionTrace() throws Exception {
	Real.verifyRealTransactionTrace();
    }

    // @Test
    public void verifyAgentStatus() throws Exception {
	Real.verifyAgentUpStatus();
    }

    // @Test
    public void createNewSLA() throws Exception {
	Real.createNewSLA(slaName);
    }

    //@Test
    public void deleteSLA() throws Exception {
	Real.deleteSLA(slaName);
    }

    // MonitorsPage.openMonitorsSummary();
    // DashboardPage.verifyMonitorDataOnDashboard(monitorName);
    // MonitorTile.checkAverageResponseTime(monitorName);
    // Navigation.openSyntheticDashboardTab();
    // MonitorTile.responseTimeChartIsDisplayed(monitorName);
    // MonitorTile.verifyMonitorAvailability(monitorName);
    // MonitorsPage.aboutThisMonitorContainsValidData();
    // MonitorsPage.lastStatusVerification();
    // MonitorsPage.lastResponseTimeVerification();
    // MonitorsPage.timeSinceLastErrorVerification();
    // MonitorsPage.availabilityHistoryVerification();
    // MonitorsPage.performanceHistoryVerification();
    // MonitorsPage.runHistoryVerification();
    // MonitorsPage.monitorBlackoutsVerification();

}
