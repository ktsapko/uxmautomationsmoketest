package com.smartbear.uxm.test.smoketest;

import org.openqa.selenium.By;
import org.testng.annotations.*;

public class AgentSetupWizard extends LoginTests {

	// Open Agent Setup Wizard
	public void openAgentSetupWizard() throws Exception {
		Driver.Instance.findElement(
				By.xpath("//div[@id='app-nav']/div[2]/div/ul/li/span")).click();
	}

	// Click on Java 1.5+ button
	public void clickOnJavaAgentButton() {
		Driver.Instance.findElement(
				By.xpath("//div[2]/div/div/div/div/div/div[2]/button")).click();
	}

	// Click on .NET 32-bit button
	public void clickOnNet32Button() {
		Driver.Instance.findElement(By.xpath("//div[2]/div[2]/button")).click();
	}

	// Click on .NET 64-bit button
	public void clickOnNet64Button() {
		Driver.Instance.findElement(By.xpath("//div[2]/div[3]/button")).click();
	}
	
	// Click on I'm done button
		public void clickOnIamDoneButton() {
			Driver.Instance.findElement(By.cssSelector("a.continueBtn > button.btn")).click();
		}

}
