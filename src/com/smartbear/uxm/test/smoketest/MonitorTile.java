package com.smartbear.uxm.test.smoketest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;

public class MonitorTile extends Driver {

    public static void checkAverageResponseTime(String monitorName) {
	SyntheticDashboardPage.searchForMonitor(monitorName);
	WebElement responseTimeContainer = Instance.findElement(By.xpath("//div[@class='asMonitorInfo ng-binding']"));
	String respTimeValue = responseTimeContainer.getAttribute("title");
	try {
	    Assert.assertTrue("Monitor run has errors", respTimeValue.endsWith("s"));
	} catch (NotFoundException e) {

	    e.printStackTrace();
	}

    }

    public static void responseTimeChartIsDisplayed(String monitorName) {
	SyntheticDashboardPage.searchForMonitor(monitorName);
	try {
	    WebElement circleIcon = Instance.findElement(By
		    .xpath("//*[@transform='translate(15,20)']//*[name()='circle']"));
	    // Assert.assertTrue("No data about performance available",
	    // circleIcon != null);
	} catch (Exception e) {
	    System.out.println("No data about performance available");
	}

    }

    public static void verifyMonitorAvailability(String monitorName) {

	SyntheticDashboardPage.searchForMonitor(monitorName);
	WebElement availabilityBarsChart = Instance.findElement(By.xpath("//*[@transform='translate(15,6)']"));
	List<WebElement> bars = availabilityBarsChart.findElements(By.xpath(".//*[name()='g']"));
	int totalCountOfbars = bars.size();
	for (WebElement RedBar : bars) {

	    if ((RedBar.getAttribute("class") == "red-rect") && (RedBar.getAttribute("height") != "0")) {
		RedBar.click();
	    }

	    else if (totalCountOfbars == 0) {
		System.out.println("Monitor has no runs");
	    }

	}

    }

    // This method wouldn't work because monitor id is dynamic. It will be
    // refreshed each 5 minute.

    // public WebElement getMonitorDataByName(String monitorName) {
    //
    // //WebElement index = null;
    // assertTrue(Instance.findElement(By.xpath("//div[@class='element-list']")).isDisplayed());
    // WebElement monitorsContainer =
    // Instance.findElement(By.xpath("//div[@class='element-list']"));
    // List<WebElement> allMonitorsList = monitorsContainer.findElements(By
    // .xpath("//div[@class='rdgItem ng-scope asMonitor medium']"));
    //
    // for (WebElement monitorItemIndex : allMonitorsList) {
    // monitorItemIndex.getAttribute("resultindex");
    // //String xpathToMonitorInGrid =
    // "//div[@class='rdgItem ng-scope asMonitor medium' and @resultindex='" +
    // monitorItemIndex + "']";
    // String xpathToMonitor = "//div[@resultindex='" + monitorItemIndex +
    // "']//*[@class='asMonitorName ng-binding' and @title='" + monitorName +
    // "']";
    //
    //
    //
    // }
    //
    // return this.monitorNameTitle;
    // }

}
