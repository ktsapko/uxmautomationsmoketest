package com.smartbear.uxm.test.smoketest;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MonitorSummary extends Driver {

    /*
     * These are methods that will be used by JUnit comment
     */
    public static void enableTransactionTrace(String monitorName) throws InterruptedException {
	LoginTests.performLogin();
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	SyntheticDashboardPage.searchForMonitor(monitorName);
	SyntheticDashboardPage.selectSyntheticMonitor(monitorName);
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@id='alertsite_monitor_availability_details_tile']/div[2]/div/div[2]/div/div/button")));
	WebElement editMonitorButton = Instance.findElement(By
		.xpath("//div[@id='alertsite_monitor_availability_details_tile']/div[2]/div/div[2]/div/div/button"));

	// Verify if transaction trace is already enabled	
	while (TransactionTraceEnabled() == false) {
	    editMonitorButton.click();
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='modalDialog']")));
	    Instance.findElement(By.xpath("//h2[contains(., 'Monitoring Settings')]")).click();
	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='transaction_trace']")));
	    WebElement tt_chekbox = Instance.findElement(By.xpath("//input[@name='transaction_trace']"));
	    if (tt_chekbox.isSelected() != true) {
		tt_chekbox.click();
	    }
	    Instance.findElement(By.xpath("//button[contains(., 'Submit')]")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='modal-header']/button")));
	    Thread.sleep(15000);
	    Instance.findElement(By.xpath("//div[@class='modal-header']/button")).click();

	}
	    Assert.assertTrue("Transaction Trace is still disabled", TransactionTraceEnabled() == true);

    }
    
    public static Boolean TransactionTraceEnabled(){
	String transactionTraceState = Instance
		.findElement(
			By.xpath("//li[@class='verticalListItem ng-scope' and @resultindex=4]//div[@class='verticalListValue ng-binding']"))
		.getText();
	if(transactionTraceState.contentEquals("Enabled")){
	    return true;
	}
	else{
	    return false;
	}
    }

    public static void runTestOnDemand(String monitorName) throws InterruptedException {
	boolean successfullRun = false;
	String actionDescription = null;
	String statusCode = null;

	LoginTests.performLogin();
	SyntheticDashboardPage.searchForMonitor(monitorName);
	SyntheticDashboardPage.selectSyntheticMonitor(monitorName);	
	
	// Start test
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(
		ExpectedConditions.elementToBeClickable(By
			.xpath("//div[@id='alertsite_monitor_availability_details_tile']/div[2]/div/div[2]/div/div/button")))
		.click();
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='modalDialog']")));
	Instance.findElement(By.xpath("//h2[contains(., 'Test On Demand')]")).click();
	Thread.sleep(3000);
	WebElement frame = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("frame-component")));
	// Instance.switchTo().defaultContent();
	Instance.switchTo().frame(frame);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("content")));
	WebElement progressBar = Instance.findElement(By.id("as_progressbar"));
	if (progressBar.isDisplayed()) {
	    String executionProgress = progressBar.getAttribute("title");
	    while (executionProgress.contentEquals("100% Complete.")) {
		try {
		    if (progressBar.isDisplayed() == true) {
			Thread.sleep(2000);
		    }
		} catch (Exception e) {
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[@class='align_r']")));
		    statusCode = Instance.findElement(By.xpath("//tbody/tr[2]/td[5]/font")).getText();
		    actionDescription = Instance.findElement(By.xpath("//*[@id='as_action_message']")).getText();
		    System.out.println(e);
		}
	    }

	} else {
	    WebElement fontColor = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//font")));
	    String color = fontColor.getAttribute("color");
	    if (color.contentEquals("green")) {
		actionDescription = Instance.findElement(By.xpath("//*[@id='as_action_message']")).getText();
		successfullRun = true;
	    } else if (color.contentEquals("red")) {
		actionDescription = Instance.findElement(By.xpath("//*[@id='as_action_message']")).getText();
		successfullRun = false;
	    }
	}
	String runResult = "Description:" + actionDescription + "Status Code:" + statusCode;
	assertTrue(runResult, successfullRun == true);
    }

    // Methods
    // Open Monitors Summary Page
    public static void openMonitorsSummary() {

	Driver.Instance.findElement(By.linkText("Monitors")).click();
	Driver.Instance.findElement(By.cssSelector("a.sub-navigation-item-link > span.ng-binding")).click();
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@id='alertsite_monitor_availability_details_tile']/div[2]/div/div[2]/div/div/button")));
    }

    public static void aboutThisMonitorContainsValidData() {
	// TODO Auto-generated method stub

    }

    public static void lastStatusVerification() {
	// TODO Auto-generated method stub

    }

    public static void lastResponseTimeVerification() {
	// TODO Auto-generated method stub

    }

    public static void timeSinceLastErrorVerification() {
	// TODO Auto-generated method stub

    }

    public static void availabilityHistoryVerification() {
	// TODO Auto-generated method stub

    }

    public static void performanceHistoryVerification() {
	// TODO Auto-generated method stub

    }

    public static void runHistoryVerification() {
	// TODO Auto-generated method stub

    }

    public static void monitorBlackoutsVerification() {
	// TODO Auto-generated method stub

    }

}
