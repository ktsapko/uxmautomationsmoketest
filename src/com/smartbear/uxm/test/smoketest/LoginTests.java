package com.smartbear.uxm.test.smoketest;

import java.util.Random;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginTests extends LoginPage {
    // WebDriver driver;
    String baseUrl;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
	Driver.Initialize();
    }

    public static void performLogin() {

	LoginPage.GoTo(Driver.baseUrl);
	LoginPage.LoginAs(Driver.userName).WithPassword(Driver.password).Login();
	// Wait for dashboard to load
	WebDriverWait wait = new WebDriverWait(Driver.Instance, 30);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='app-nav']/ul/li[3]/a/span/span")));
    }

    public static void WaitForDashboardToLoad() {
	WebDriverWait wait = new WebDriverWait(Driver.Instance, 30);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='app-nav']/ul/li[3]/a/span/span")));
    }

    @After
    public void tearDown() throws Exception {
	Driver.Instance.quit();
	String verificationErrorString = verificationErrors.toString();
	if (!"".equals(verificationErrorString)) {
	    fail(verificationErrorString);
	}
    }

    public boolean isElementPresent(By by) {
	try {
	    Driver.Instance.findElement(by);
	    return true;
	} catch (NoSuchElementException e) {
	    return false;
	}
    }
    
    // Random string generator
    public static String randomName() {
	char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
	StringBuilder sb = new StringBuilder();
	Random random = new Random();
	for (int i = 0; i < 20; i++) {
	    char c = chars[random.nextInt(chars.length)];
	    sb.append(c);
	}
	String output = sb.toString();
	return output;
    }
}
