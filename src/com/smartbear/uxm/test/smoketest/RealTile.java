package com.smartbear.uxm.test.smoketest;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;

public class RealTile extends Driver {

    public static void verifyApdexIsAvailable(String applicationName) throws Exception {

	Real.searchForApplication(applicationName);
	WebElement apdexContainer = Instance.findElement(By.xpath("//div[@class='realMonitorInfo ng-binding']"));
	String apcexValue = apdexContainer.getAttribute("title");
	try {
	    Assert.assertTrue("APDEX is not available", apcexValue.endsWith("*"));
	} catch (NotFoundException e) {

	    e.printStackTrace();
	}

    }

    public static void verifyRealMonitorGraphIsAvailable(String applicationName) throws Exception {
	Real.searchForApplication(applicationName);
	WebElement realMonitoringBarsChart = Instance.findElement(By.xpath("//*[@transform='translate(15,0)']"));
	List<WebElement> bars = realMonitoringBarsChart.findElements(By.xpath(".//*[name()='g']"));
	int totalCountOfbars = bars.size();
	for (WebElement RedBar : bars) {

	    if ((RedBar.getAttribute("class") == "red-rect") && (RedBar.getAttribute("height") != "0")) {
		RedBar.click();
	    }

	    else if (totalCountOfbars == 0) {
		System.out.println("Monitor has no Real Transactions");
	    }
	}

    }
    public static void verifyApplicationDataInRealTile() throws Exception {
	LoginTests.performLogin();
   	Navigation.openScreenReal();
   	Thread.sleep(1000);
   	Navigation.openRealDashboardTab();
   	//Real.searchForApplication(applicationName);
   	verifyApdexIsAvailable(applicationName);
   	verifyRealMonitorGraphIsAvailable(applicationName);

       }
    
}
