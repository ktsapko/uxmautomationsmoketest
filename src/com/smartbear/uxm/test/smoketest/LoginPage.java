package com.smartbear.uxm.test.smoketest;

public class LoginPage{
	

	public static void GoTo(String baseUrl) {

		Driver.Instance.get(baseUrl);
	}

	public static LoginCommand LoginAs(String userName) {

		return new LoginCommand(userName);
	}

}
