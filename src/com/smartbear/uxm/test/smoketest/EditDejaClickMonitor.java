package com.smartbear.uxm.test.smoketest;

import org.junit.*;

import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditDejaClickMonitor extends Driver {

    public EditDejaClickMonitor() throws Exception {

	Instance.findElement(
		By.xpath("//div[@id='alertsite_monitor_availability_details_tile']/div[2]/div/div[2]/div/div/button"))
		.click();
	assertTrue(isElementPresent(By.xpath("//div[@class='modalDialog']")));
	Instance.findElement(By.xpath("//h2[contains(., 'Monitoring Settings')]")).click();
	Instance.findElement(By.xpath("//h2[contains(., 'Alert Recipients')]")).click();
	Instance.findElement(By.xpath("//h2[contains(., 'Test On Demand')]")).click();
	Instance.findElement(By.xpath("//h2[contains(., 'Manage Locations')]")).click();
	Instance.findElement(By.cssSelector("div.modal-header > button.close")).click();
	Instance.findElement(By.xpath("//a[@class='deleteLink deleteLinkBFX']")).click();
	Instance.findElement(By.xpath("//button[contains(., 'Cancel')]")).click();
	Instance.findElement(By.xpath("//button[contains(., 'Submit')]")).click();

	// Monitoring tab actions
	Instance.findElement(By.linkText("Monitoring")).click();
	assertTrue(Instance
		.findElement(
			By.xpath("//div[contains(., 'Selected fullpage interval is not a multiple of selected monitoring interval')]"))
		.isDisplayed());
	Instance.findElement(By.xpath("//input[@name='name']")).clear();
	Instance.findElement(By.xpath("//input[@name='name']")).sendKeys("");
	new Select(Instance.findElement(By.xpath("//select[@name='interval']"))).selectByVisibleText("1");
	new Select(Instance.findElement(By.xpath("//select[@name='timeout']"))).selectByVisibleText("20");
	Instance.findElement(By.xpath("//input[@name='enabled']")).click();
	Instance.findElement(By.xpath("//input[@name='notify_on_error']")).click();
	Instance.findElement(By.xpath("//input[@name='transaction_trace']")).click();

	// Advanced tab actions
	Instance.findElement(By.linkText("Advanced")).click();
	new Select(Instance.findElement(By.xpath("//select[@name='capture_level']")))
		.selectByVisibleText("All Actions");
	new Select(Instance.findElement(By.xpath("//select[@name='mode']"))).selectByVisibleText("Primary");
	Instance.findElement(By.xpath("//input[@name='retry_on_failure']")).click();
	Instance.findElement(By.xpath("//input[@name='step_timeout']")).sendKeys("12");
	Instance.findElement(By.xpath("//input[@name='continue_playback_on_timeout']")).click();
	Instance.findElement(By.xpath("//input[@name='notify_on_content_change']")).click();
	Instance.findElement(By.xpath("//input[@name='traceroute_on_error']")).click();
	Instance.findElement(By.xpath("//input[@name='allow_internal_testing']")).click();

	// Full page tab actions
	Instance.findElement(By.linkText("Fullpage")).click();
	new Select(Instance.findElement(By.xpath("//select[@name='interval_fullpage']"))).selectByVisibleText("1");
	new Select(Instance.findElement(By.xpath("//select[@name='fullpage_object_timeout']")))
		.selectByVisibleText("1");
	Instance.findElement(By.xpath("//input[@name='notify_on_fullpage_errors']")).click();
	Instance.findElement(By.xpath("//input[@name='check_fullpage_missing_objects']")).click();
	Instance.findElement(By.xpath("//input[@name='check_fullpage_object_sizes']")).click();

	// User Experience tab actions
	Instance.findElement(By.linkText("User Experience")).click();
	Instance.findElement(By.xpath("//input[@name='user_experience_enabled']")).click();
	Instance.findElement(By.xpath("//input[@name='user_experience_base_page_only']")).click();
    }

}
