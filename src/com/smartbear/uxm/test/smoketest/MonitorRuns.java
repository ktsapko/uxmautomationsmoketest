package com.smartbear.uxm.test.smoketest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class MonitorRuns extends Driver {

    public static void verifySyntheticTransactionTraceData(String monitorName) throws Exception {

	LoginTests.performLogin();
	SyntheticDashboardPage.selectSyntheticMonitor(monitorName);
	Navigation.openRunsTab();
	selectRun();
	selectStep();
	selectEvent();
	syntheticTransactionsArePresent();
    }

    private static void syntheticTransactionsArePresent() {

	// Select jsp with transaction trace
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	try {
	    wait.until(ExpectedConditions.elementToBeClickable(By
		    .xpath("//div[@class='waterfallReport']/table/tbody/tr")));
	    WebElement transactionTraceTable = Instance.findElement(By
		    .xpath("//div[@class='waterfallReport']/table/tbody"));
	    List<WebElement> transactionTraceContainer = transactionTraceTable.findElements(By.tagName("tr"));
	    WebElement firstTransaction = transactionTraceContainer.get(0).findElement(By.xpath("//td[1]/i"));
	    firstTransaction.click();
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By
		    .xpath("//tr[@class='runsTimelineContainer nonClickable ng-scope']")));
	    assertTrue(Instance.findElement(By.xpath("//tr[@class='runsTimelineContainer nonClickable ng-scope']"))
		    .isDisplayed());
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    private static void selectEvent() {

	// Select first event
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	try {
	    wait.until(ExpectedConditions.elementToBeClickable(By
		    .xpath("//div[@class='stepContainerCell']/table[1]/tbody/tr[2]/td/div/table/tbody/tr")));
	    WebElement eventTable = Instance.findElement(By
		    .xpath("//div[@class='stepContainerCell']/table[1]/tbody/tr[2]/td/div/table/tbody"));
	    List<WebElement> eventElementContainer = eventTable.findElements(By.tagName("tr"));
	    WebElement firstEvent = eventElementContainer.get(0);
	    firstEvent.click();
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='mainContainer']")));
	    assertTrue(Instance.findElement(By.xpath("//div[@class='mainContainer']")).isDisplayed());
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    private static void selectStep() {
	// Select first step
	WebDriverWait wait = new WebDriverWait(Instance, 20);
	try {
	    wait.until(ExpectedConditions.elementToBeClickable(By
		    .xpath("//div[@class='stepContainerCell']/table[@class='ng-isolate-scope']/tbody/tr")));
	    WebElement stepTable = Instance.findElement(By
		    .xpath("//div[@class='stepContainerCell']/table[@class='ng-isolate-scope']/tbody"));
	    List<WebElement> stepsList = stepTable.findElements(By.tagName("tr"));
	    WebElement firstStep = stepsList.get(0);
	    firstStep.click();
	    wait.until(ExpectedConditions.elementToBeClickable(By
		    .xpath("//div[@class='stepContainerCell']/table[1]/tbody/tr[2]/td/div/table/tbody/tr")));
	    assertTrue(Instance.findElement(
		    By.xpath("//div[@class='stepContainerCell']/table[1]/tbody/tr[2]/td/div/table/tbody/tr"))
		    .isDisplayed());
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    private static void selectRun() throws InterruptedException {

	WebDriverWait wait = new WebDriverWait(Instance, 20);
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//body/div[2]/div[2]/div[2]/div[2]/div/div[3]/table/tbody")));
	WebElement runsTable = Instance.findElement(By
		.xpath("//body/div[2]/div[2]/div[2]/div[2]/div/div[3]/table/tbody"));
	List<WebElement> runsList = runsTable.findElements(By.tagName("tr"));
	// Thread.sleep(999);
	WebElement lastRun = runsList.get(0);
	// Thread.sleep(999);
	lastRun.click();
	wait.until(ExpectedConditions.elementToBeClickable(By
		.xpath("//div[@class='stepContainerCell']/table[@class='ng-isolate-scope']/tbody/tr")));
	assertTrue(Instance.findElement(
		By.xpath("//div[@class='stepContainerCell']/table[@class='ng-isolate-scope']/tbody/tr")).isDisplayed());

    }

}
