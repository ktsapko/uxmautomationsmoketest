package com.smartbear.uxm.test.smoketest;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.*;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.*;

public class Driver {

    // Test prerequisites:
    // Credentials
    protected static String userName = "smartbtestacc4@gmail.com";
    protected static String password = "smartb3ar";

    // Environment URL
    static String baseUrl = "https://uxm.qac.alertsite.com/ui";
    static String apiMonitorName = "API monitor by WebDriver";
    static String webSiteMonitorName = "Web Site monitor by WebDriver";
    static String sslWebSiteMonitorName = "SSL Web Site monitor by WebDriver";
    static String applicationName = "jira";

    static WebDriver Instance;

    public WebDriver getInstance() {
	return Driver.Instance;
    }

    public void setInstance(WebDriver driver) {
	Driver.Instance = driver;
	Instance.manage().window().maximize();
    }

    public static void Initialize() {
	Instance = new FirefoxDriver();
	Instance.manage().window().maximize();
	// Instanace.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
    }

    public static boolean isElementPresent(By by) {
	try {
	    Instance.findElement(by);
	    return true;
	} catch (NoSuchElementException e) {
	    return false;
	}
    }

}
