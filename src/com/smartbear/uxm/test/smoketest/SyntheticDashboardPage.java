package com.smartbear.uxm.test.smoketest;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.*;

public class SyntheticDashboardPage extends Driver {

    // Create Web Site Monitor

    public static void selectSyntheticMonitor(String monitorName) throws InterruptedException {

	WebDriverWait wait = new WebDriverWait(Instance, 30);
	String xpathMonitorName = "//*[@class='asMonitorName ng-binding' and @title='" + monitorName + "']";
	Instance.findElement(By.xpath(xpathMonitorName)).click();
	wait.until(ExpectedConditions.visibilityOfElementLocated(By
		.xpath("//div[@class='verticalListValue ng-binding']")));
	Thread.sleep(3000);
    }

    public static void selectOrganization() {

	Instance.findElement(By.xpath("//a[@title='RightHere']")).click();
    }

    public static void searchForMonitor(String monitorName) {
	Instance.findElement(By.name("searchInput")).clear();
	Instance.findElement(By.name("searchInput")).sendKeys(monitorName);

    }

    public static void createWebSiteMonitor(String monitorName, String urlToMonitor, String runInterval, String timeout)
	    throws InterruptedException {

	LoginTests.performLogin();
	WebDriverWait wait = new WebDriverWait(Instance, 30);
	WebElement newMonitorButton = Instance.findElement(By.xpath("//button[@ng-click='clickButton()']"));
	newMonitorButton.click();
	Instance.findElement(By.xpath("//td[2]/div/h1")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tbody/tr/td[3]")));
	Instance.findElement(By.xpath("//tbody/tr/td[3]")).click();
	Instance.findElement(By.xpath("//div[3]/div/button[2]")).click();
	for (int second = 0;; second++) {
	    if (second >= 60)
		fail("timeout");
	    try {
		if ("".equals(Instance.findElement(By.xpath("//input[@name='name']")).getText()))
		    break;
	    } catch (Exception e) {
	    }
	    Thread.sleep(1000);
	}
	// Fill in monitor name
	Instance.findElement(By.xpath("//input[@name='name']")).clear();
	WebElement monitorNameField = Instance.findElement(By.xpath("//input[@name='name']"));
	monitorNameField.sendKeys(monitorName);
	// Fill in URL to monitor
	Instance.findElement(By.xpath("//input[@name='url']")).clear();
	WebElement urlToMonitorField = Instance.findElement(By.xpath("//input[@name='url']"));
	urlToMonitorField.sendKeys(urlToMonitor);
	// Set Run Interval
	new Select(Instance.findElement(By.xpath("//select[@name='interval']"))).selectByVisibleText(runInterval);
	// Set monitor run timeout
	new Select(Instance.findElement(By.xpath("//select[@name='timeout']"))).selectByVisibleText(timeout);
	Instance.findElement(By.xpath("//div[3]/div/button[2]")).click();
	Thread.sleep(10000);

	// Wait for monitor options modal window and close it
	WebDriverWait waitForModalWindows = new WebDriverWait(Instance, 15);
	WebElement modalWindow = Instance.findElement(By.xpath("//div[@class='modalDialog']"));
	waitForModalWindows.until(ExpectedConditions.visibilityOf(modalWindow));
	Instance.findElement(By.cssSelector("div.modal-header > button.close")).click();
	Thread.sleep(1000);

	// Verify Monitor name
	String xpathToMonitorName = "//*[@class='asMonitorName ng-binding' and @title='" + monitorName + "']";
	searchForMonitor(monitorName);
	Thread.sleep(1000);
	WebElement monitorNameOnDashboard = Instance.findElement(By.xpath(xpathToMonitorName));
	Assert.assertTrue("Monitor name was not found", monitorNameOnDashboard != null);

	// Verify Monitor Type
	WebElement monitorTypeOnDashboard = Instance.findElement(By.xpath("//*[@class='asMonitorFooter']//i"));
	Assert.assertTrue("Monitor type was not found", monitorTypeOnDashboard != null);

	// Verify Last Run
	WebElement monitorLastRunOnDashboard = Instance.findElement(By
		.xpath("//*[@class='asMonitorFooter']//span[@class='asMonitorLastRun']"));
	Assert.assertTrue("Monitor last run was not found", monitorLastRunOnDashboard != null);

	// Verify Interval
	WebElement monitorRunIntervalOnDashboard = Instance.findElement(By
		.xpath("//*[@class='asMonitorFooter']//span[@class='asMonitorInterval']"));
	Assert.assertTrue("Monitor interval was not found", monitorRunIntervalOnDashboard != null);

    }

    // Optional case if account has multiple organizations after authorization

    // Secure Web Site Monitor
    public static void createSecureWebsiteMonitor(String monitorName, String urlToMonitor, String runInterval,
	    String timeout) throws InterruptedException {

	LoginTests.performLogin();
	WebDriverWait wait = new WebDriverWait(Instance, 30);
	WebElement newMonitorButton = Instance.findElement(By.xpath("//button[@ng-click='clickButton()']"));
	newMonitorButton.click();
	// Select Secure Web Site Monitor
	Instance.findElement(By.xpath("//tr[2]/td[2]/div/h1")).click();
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tbody/tr/td[3]"))).click();
	// Instance.findElement(By.xpath("//tbody/tr/td[3]")).click();
	Instance.findElement(By.xpath("//div[3]/div/button[2]")).click();
	for (int second = 0;; second++) {
	    if (second >= 60)
		fail("timeout");
	    try {
		if ("".equals(Instance.findElement(By.xpath("//input[@name='name']")).getText()))
		    break;
	    } catch (Exception e) {
	    }
	    Thread.sleep(1000);
	}

	// Fill in monitor name
	Instance.findElement(By.xpath("//input[@name='name']")).clear();
	WebElement monitorNameField = Instance.findElement(By.xpath("//input[@name='name']"));
	monitorNameField.sendKeys(monitorName);

	// Fill in URL to monitor
	Instance.findElement(By.xpath("//input[@name='url']")).clear();
	WebElement urlToMonitorField = Instance.findElement(By.xpath("//input[@name='url']"));
	urlToMonitorField.sendKeys(urlToMonitor);

	// Set Run Interval
	new Select(Instance.findElement(By.xpath("//select[@name='interval']"))).selectByVisibleText(runInterval);

	// Set monitor run timeout and finish
	new Select(Instance.findElement(By.xpath("//select[@name='timeout']"))).selectByVisibleText(timeout);
	Instance.findElement(By.xpath("//div[3]/div/button[2]")).click();
	Thread.sleep(10000);

	// Wait for monitor options modal window and close it
	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='modalDialog']")));
	Instance.findElement(By.cssSelector("div.modal-header > button.close")).click();
	Thread.sleep(3000);

	// Verify Monitor name
	String xpathToMonitorName = "//*[@class='asMonitorName ng-binding' and @title='" + monitorName + "']";
	searchForMonitor(monitorName);
	Thread.sleep(1000);
	WebElement monitorNameOnDashboard = Instance.findElement(By.xpath(xpathToMonitorName));
	Assert.assertTrue("Monitor name was not found", monitorNameOnDashboard != null);

	// Verify Monitor Type
	WebElement monitorTypeOnDashboard = Instance.findElement(By.xpath("//*[@class='asMonitorFooter']//i"));
	Assert.assertTrue("Monitor type was not found", monitorTypeOnDashboard != null);

	// Verify Last Run
	WebElement monitorLastRunOnDashboard = Instance.findElement(By
		.xpath("//*[@class='asMonitorFooter']//span[@class='asMonitorLastRun']"));
	Assert.assertTrue("Monitor last run was not found", monitorLastRunOnDashboard != null);

	// Verify Interval
	WebElement monitorRunIntervalOnDashboard = Instance.findElement(By
		.xpath("//*[@class='asMonitorFooter']//span[@class='asMonitorInterval']"));
	Assert.assertTrue("Monitor interval was not found", monitorRunIntervalOnDashboard != null);

    }

    // API Monitor
    public static void createAPIMonitor(String monitorName, String runInterval, String timeout)
	    throws InterruptedException {

	LoginTests.performLogin();
	WebDriverWait wait = new WebDriverWait(Instance, 40);
	WebElement newMonitorButton = Instance.findElement(By.xpath("//button[@ng-click='clickButton()']"));
	newMonitorButton.click();
	WebElement apiMonitorButton = Instance.findElement(By.xpath("//tr[4]/td[2]/div/p"));
	wait.until(ExpectedConditions.elementToBeClickable(apiMonitorButton)).click();

	wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(., 'Usage Based Monitoring')]")))
		.click();
	WebElement NextStepButton = Instance.findElement(By.xpath("//div[3]/div/button[2]"));
	NextStepButton.click();
	try {
	    Instance.findElement(By.cssSelector("input[type=\"file\"]")).sendKeys(
		    "C:\\Users\\kit\\Documents\\Jira_208.115.48.20IP.zip");
	    NextStepButton.click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@name='selectTestCaseRadio'])")))
		    .click();
	    // Second radio button. Saved as an example of xpath navigation.
	    // Instance.findElement(By.xpath("(//input[@name='selectTestCaseRadio'])[2]")).click();
	    NextStepButton.click();
	} catch (Exception e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	// Input monitor options
	// Fill in monitor name
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='name']")));
	Instance.findElement(By.xpath("//input[@name='name']")).clear();
	WebElement monitorNameField = Instance.findElement(By.xpath("//input[@name='name']"));
	monitorNameField.sendKeys(monitorName);

	// Set Run Interval
	new Select(Instance.findElement(By.xpath("//select[@name='interval']"))).selectByVisibleText(runInterval);

	// Set monitor run timeout and finish
	Instance.findElement(By.xpath("//input[@name='timeout']")).clear();
	WebElement apiMonitorTimeout = Instance.findElement(By.xpath("//input[@name='timeout']"));
	apiMonitorTimeout.sendKeys(timeout);
	Instance.findElement(By.xpath("//div[3]/div/button[2]")).click();

	// Wait for monitor options modal window and close it
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Finish without testing"))).click();
	Thread.sleep(10000);

	// Verify Monitor name
	String xpathToMonitorName = "//*[@class='asMonitorName ng-binding' and @title='" + monitorName + "']";
	searchForMonitor(monitorName);
	WebElement monitorNameOnDashboard = Instance.findElement(By.xpath(xpathToMonitorName));
	Assert.assertTrue("Monitor name was not found", monitorNameOnDashboard != null);

	// Verify Monitor Type
	WebElement monitorTypeOnDashboard = Instance.findElement(By.xpath("//*[@class='asMonitorFooter']//i"));
	Assert.assertTrue("Monitor type was not found", monitorTypeOnDashboard != null);

	// Verify Last Run
	WebElement monitorLastRunOnDashboard = Instance.findElement(By
		.xpath("//*[@class='asMonitorFooter']//span[@class='asMonitorLastRun']"));
	Assert.assertTrue("Monitor last run was not found", monitorLastRunOnDashboard != null);

	// Verify Interval
	WebElement monitorRunIntervalOnDashboard = Instance.findElement(By
		.xpath("//*[@class='asMonitorFooter']//span[@class='asMonitorInterval']"));
	Assert.assertTrue("Monitor interval was not found", monitorRunIntervalOnDashboard != null);

    }

    public void AssertUserHasNoMonitorPlan() throws Exception {
	assertEquals("No data available",
		Instance.findElement(By.xpath("//div[@class='alert alert-info ng-scope ng-binding']")).getText());
    }

    public static void verifyMonitorDataOnDashboard(String monitorName) {

	// // Search for monitor name
	// Instance.findElement(By.name("searchInput")).clear();
	// Instance.findElement(By.name("searchInput")).sendKeys(monitorName);

	// // Get the list of all monitors
	WebElement monitorsList = (WebElement) Instance.findElement(By.xpath("//*[@class='element-list']"));
	String monitorXpathLocator = "//*[@class='rdgItem ng-scope asMonitor medium']//*[@class='asMonitorHeader']//*[@title='"
		+ monitorName + "']";
	assertTrue(isElementPresent(By.xpath(monitorXpathLocator)));
	assertTrue(isElementPresent(By
		.xpath("//*[@class='rdgItem ng-scope asMonitor medium']//*[@class='asMonitorDetails']//*[@class='chart1']")));
	assertTrue(isElementPresent(By
		.xpath("//*[@class='rdgItem ng-scope asMonitor medium']//*[@class='asMonitorFooter']")));
	// WebElement monitorDetails = (WebElement) Instance.findElement(By
	// .xpath("//*[@class='asMonitorName ng-binding' and @title='JA8_Cash_tans']"));

    }

}
