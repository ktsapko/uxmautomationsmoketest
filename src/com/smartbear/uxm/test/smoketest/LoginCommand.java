package com.smartbear.uxm.test.smoketest;

import org.openqa.selenium.By;
import org.testng.annotations.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginCommand {
	
	private String userName;
	private String password;
	
	
	public LoginCommand(String userName){
		
		this.userName = userName;
	}

	public LoginCommand WithPassword(String password) {		
		this.password = password;
		return this;
	}

	public void Login() {
		
		WebElement loginInput = Driver.Instance.findElement(By.id("id_email"));
		loginInput.sendKeys(userName);
		WebElement passwordinput = Driver.Instance.findElement(By.id("id_password"));
		passwordinput.sendKeys(password);
		WebElement loginButton = Driver.Instance.findElement(By.xpath("//input[@value='Sign In']"));
		loginButton.click();
		
		
	}

}
